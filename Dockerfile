FROM tomaslin/spring-cli
EXPOSE 8080
ADD . /usr/local/app
WORKDIR /usr/local/app
ENV HOME /usr/local/app
RUN spring grab app.groovy
CMD ["spring","run","app.groovy","--","--spring.data.mongodb.host=${MONGO_PORT_27017_TCP_ADDR}"]