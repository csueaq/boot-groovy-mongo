@Grab("org.grails:gorm-mongodb-spring-boot:1.0.0.RC1")
import grails.persistence.*
import grails.mongodb.geo.*
import org.bson.types.ObjectId
import org.springframework.http.*
import org.springframework.data.annotation.Id
import org.springframework.beans.factory.annotation.Autowired
import static org.springframework.web.bind.annotation.RequestMethod.*
import org.springframework.data.mongodb.core.mapping.MongoMappingContext
@RestController
class TestController {

    @Bean
    MongoMappingContext springDataMongoMappingContext() {
        return new MongoMappingContext()
    }
    @RequestMapping(value="/", method = GET)
    Integer index() {

        //println Test.list().size()

        return Test.list().collect{it.stockValue * 1.1}.sum()

    }

}

@Entity
class Test {

    static mapping = {
        collection "stock"
        database "local"
    }
    @Id
    String id 
    int stockValue

    static constraints = {
        stockValue blank:false
    }
    

    
}
